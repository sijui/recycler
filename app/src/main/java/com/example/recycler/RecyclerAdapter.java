package com.example.recycler;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.RecyclerAdapterViewHolder> {

    Context context;
    List<DataModel> lists;
    public RecyclerAdapter(Context context,List<DataModel> dataModels){

        this.context=context;
        this.lists=dataModels;

    }
    @NonNull
    @Override
    public RecyclerAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view= LayoutInflater.from(context).inflate(R.layout.adapter_layout,viewGroup,false);
        return new RecyclerAdapterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerAdapterViewHolder holder, int i) {

        DataModel dataModel=lists.get(i);
        holder.testedData.setText(dataModel.Kiswahili_name);
    }

    public void updateData(List<DataModel> updatedData){
        this.lists = updatedData;
        this.notifyDataSetChanged();
    }


    @Override
    public int getItemCount() {
        return lists.size();
    }

    public class RecyclerAdapterViewHolder extends RecyclerView.ViewHolder{

        TextView testedData;
        public RecyclerAdapterViewHolder(@NonNull View itemView) {
            super(itemView);
            testedData=itemView.findViewById(R.id.text2);
        }
    }
}
