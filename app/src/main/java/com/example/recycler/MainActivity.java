package com.example.recycler;


import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.recycler.fragments.ImportFragments;


public class MainActivity extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();

        //instatiating the fragment
        fragmentTransaction.replace(R.id.main_frame, new ImportFragments(),"Import Fragments").commit();

    }

}
