package com.example.recycler;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

@Table(name="datamodel")



public class DataModel extends Model {
    @Column (name="Kiswahili_name")
    public String Kiswahili_name;



    @Column(name="English_name")
    public String English_name;
}
