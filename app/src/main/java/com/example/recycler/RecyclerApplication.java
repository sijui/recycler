package com.example.recycler;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.app.Application;

public class RecyclerApplication extends Application {

    @Override
  public void onCreate() {
        super.onCreate();

        ActiveAndroid.initialize(this);
    }

}

