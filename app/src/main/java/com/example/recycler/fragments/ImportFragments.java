package com.example.recycler.fragments;

import android.content.ContentValues;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.activeandroid.ActiveAndroid;
import com.example.recycler.DataModel;
import com.example.recycler.MainActivity;
import com.example.recycler.R;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class ImportFragments extends Fragment {

    Button import_btn , next_btn;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.import_fargment,container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        import_btn=view.findViewById(R.id.import_button);
        next_btn=view.findViewById(R.id.next_button);

        import_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String mCSVfile = "Sample_kiswahili-database.csv";
                AssetManager manager = getActivity().getAssets();
                InputStream inStream = null;
                try {
                    inStream = manager.open(mCSVfile);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                BufferedReader buffer = new BufferedReader(new InputStreamReader(inStream));

                String line = "";
                ActiveAndroid.beginTransaction();

                try {
                    while ((line = buffer.readLine()) != null) {
                        String[] colums = line.split(",");
                        if (colums.length != 2) {
                            Log.d("CSVParser", "Skipping Bad CSV Row");
                            continue;
                        }
                        ContentValues cv = new ContentValues(2);

                        DataModel data = new DataModel();
                        data.Kiswahili_name = colums[0].trim();
                        data.English_name = colums[1].trim();
                        data.save();
                          Log.e("ghyj",data.English_name);
                    }
                    ActiveAndroid.setTransactionSuccessful();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                finally {
                    ActiveAndroid.endTransaction();
                }
            }
        });

        next_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //type casting the main activity to allow fragment swap
                FragmentTransaction fragmentTransaction = ((MainActivity)getActivity()).getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.main_frame,new FormInputFragment(),"FormInputFragments").commit();

            }
        });
    }
}
