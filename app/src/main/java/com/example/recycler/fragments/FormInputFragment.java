package com.example.recycler.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.example.recycler.MainActivity;
import com.example.recycler.R;

public class FormInputFragment extends Fragment {

    Button submit_btn;
    EditText editText;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.forminput_fargment, container,false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        submit_btn = view.findViewById(R.id.submit_button);
        editText = view.findViewById(R.id.word);

        submit_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = editText.getText().toString();
                if(text!=null){


                    DisplayFragment displayFragment = new DisplayFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString("searchKey",text);
                    //bundle is used to pass data from one fragment to another
                    displayFragment.setArguments(bundle);

                    FragmentTransaction fragmentTransaction = ((MainActivity)getActivity()).getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.main_frame,displayFragment,"DisplayFragment").commit();
                }
                else{
                    return;
                }
            }
        });
    }
}
