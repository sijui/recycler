package com.example.recycler.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.activeandroid.query.Select;
import com.example.recycler.DataModel;
import com.example.recycler.MainActivity;
import com.example.recycler.R;
import com.example.recycler.RecyclerAdapter;

import java.util.ArrayList;
import java.util.List;

public class DisplayFragment extends Fragment {

    RecyclerAdapter recyclerAdapter;
    RecyclerView recyclerView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.display_fargment,container,false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        String search = getArguments().getString("searchKey");

        recyclerView = view.findViewById(R.id.recycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerAdapter = new RecyclerAdapter(getActivity(),new ArrayList<DataModel>());
        recyclerView.setAdapter(recyclerAdapter);

        Toast.makeText(getActivity(),search,Toast.LENGTH_LONG).show();
//        List<DataModel> listwithvalues = new Select().all().from(DataModel.class).limit(50).execute();
        List<DataModel> listwithvalues = new Select().from(DataModel.class).where("English_name=?",search).execute();
        recyclerAdapter.updateData(listwithvalues);


        Log.e("tgtg",""+listwithvalues.size());
    }
}
